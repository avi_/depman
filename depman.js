const path = require("path");
const _ = require("lodash/collection");

const H = require("./src/helpers/helpers");
const Interface = require("./src/interface/interface");
const Dependency = require("./src/types/Dependency");
const DependencyStore = require("./src/types/DependencyStore");
const packageJson = require(path.resolve(process.cwd(), "./package.json"));

const DepMan = class DepMan {
    constructor(intrface, dependencyStore, config) {
        this.Interface = intrface;
        this.dependencyStore = dependencyStore;
        this.config = {
            dependencies: config.dependencies || [],
            source: config.source || this.Interface.source || "/"
        };

        this.initializeConfig(this.config);
    }

    createDependency(name, path, truth) {
        return new Promise((resolve) => {
            resolve(new Dependency(name, path, truth));
        });
    }

    saveDependency(dependency, store) {
        return new Promise((resolve) => {
            resolve(store.add(dependency));
        });
    }

    initializeConfig(config) {
        return new Promise(((resolve, reject) => {
            _.map(config.dependencies, (item) => {
                this.dependencyStore.add(item);
                resolve(true);
            });
        }));
    }

    run() {
        // look for items we should be managing
        H.searchFiles(this.dependencyStore.store)
            .then(missingFiles => {
                // handle missing files
                H.createFiles(missingFiles);

                return H.getMissingItems(this.dependencyStore.store, missingFiles);
            })
            .then(existingFiles => {
                // diff existing to Truth and report
                // console.log(existingFiles)
            });
    }
};

module.exports = DepMan;

new DepMan(new Interface(), new DependencyStore(), packageJson.config.depman).run();