module.exports = class Dependency extends Object {
    constructor(name, path, truth) {
        super();
        this.name = name;
        this.path = path;
        this.truth = truth;
    }
};