const Dependency = require("./Dependency");

module.exports = class DependencyStore extends Array {
    constructor() {
        super();
        this.store = [];
    }

    add(dependency) {
        this.store.push(dependency);
    }

    getByName(name) {
        return _.map(this.store, (item) => item.name === name);
    }
};