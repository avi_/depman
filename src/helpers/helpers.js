const _ = require("lodash");
const path = require("path");
const fs = require("fs");
const axios = require("axios");
require("colors");

const checkFile = (targetPath) => {
    return new Promise((resolve, reject) => {
        fs.access(targetPath, fs.constants.F_OK, (error) => {
            if (error) {
                reject(error);
            }

            resolve(true);
        });
    });
};

const searchFiles = (files) => {
    const missingFiles = [];

    return new Promise((resolve, reject) => {
        _.map(files, (item, index, collection) => {
            const filePath = path.resolve(item.path, item.name);
            checkFile(filePath)
                .then(() => {
                    console.log(`${item.name} OK!`);
                    // file exists, now lets compare contents against Truth
                })
                .catch((error) => {
                    // if error because file doesn't exist, we'll need to report it
                    if (error.code === "ENOENT") {
                        missingFiles.push(item);
                    } else {
                        // TODO: handle read errors?
                        reject(error);
                    }
                })
                .finally(() => {
                    // after all dependencies are searched, report any missing files
                    if (missingFiles.length > 0) {
                        const missingFileNames = _.map(missingFiles, (item) => item.name);
                        console.log("Missing Files: ".red.bold + missingFileNames.join(", "))
                    } else {
                        console.log("No files are missing".green.bold)
                    }

                    resolve(missingFiles);
                });
        });
    });
};

const writeFile = (name, content, encoding) => {
    const _path = name[0] === "/" ? name.substring(1) : name;
    const data = content;
    const enc = encoding || "utf8";

    console.log(`writing ${name}...`);

    return new Promise((resolve,reject) => {
        fs.writeFile(path.normalize(_path), data, enc, (error) => {
            if (error) reject(error);
            resolve();
        });
    });
};

const createFiles = (files) => {
    _.map(files, (item, index, collection) => {
        console.log(`retrieving ${item.name}...`);
        axios.get(item.truth)
            .then(response => response.data)
            .then(data => {
                const _path = path.resolve(process.cwd(), item.path, item.name);
                writeFile(_path, data)
                    .catch(error => console.log(error));
            })
    });
};

const getMissingItems = (expectedList, inputList) => {
    const targetSet = new Set(inputList);

    return expectedList.filter(x => !targetSet.has(x));
};

module.exports = {
    checkFile,
    searchFiles,
    writeFile,
    createFiles,
    getMissingItems
};