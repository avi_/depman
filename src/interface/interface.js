const { version, description } = require("../../package.json");

module.exports = class Interface {
    initializeProgram() {
        this.Instance = require('commander');;
    }

    constructor() {
        this.initializeProgram();

        this.Instance
            .description(description)
            .option("-s, --source [relative path]", `[relative path]/package.json with a depman config object`)
            .version(version, "-v, --version")
            .parse(process.argv)

        return this.Instance;
    }
};