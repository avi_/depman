const fs = require("fs");
const path = require("path");
const _ = require("lodash/collection");
const axios = require("axios");
const Dependency = require("./src/types/Dependency");
const Helpers = require("./src/helpers/helpers");
require("colors");

const checkFile = Helpers.checkFile;
const searchFiles = Helpers.searchFiles;
const writeFile = Helpers.writeFile;
const createFiles = Helpers.createFiles;
const getMissingItems = Helpers.getMissingItems;

const OPTIONS = {
    rootDir: path.resolve(__dirname),
    filesToManage: []
};

const missingFiles = [];

// seeding dev data because lazy
OPTIONS.filesToManage.push(new Dependency(".npmrc", OPTIONS.rootDir, "https://gitlab.com/avi_/depman/raw/master/test/.npmrc"));


// look for items we should be managing
searchFiles(OPTIONS.filesToManage)
    .then(missingFiles => {
        // handle missing files
        createFiles(missingFiles);

        return getMissingItems(OPTIONS.filesToManage, missingFiles);
    })
    .then(existingFiles => {
        // diff existing to Truth and report
        // console.log(existingFiles)
    });
